package com.example.myapplication.ui.blutdruck;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class BlutdruckViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public BlutdruckViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Blutdruck");
    }

    public LiveData<String> getText() {
        return mText;
    }
}