package com.example.myapplication.ui.blutzucker;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class BlutzuckerViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public BlutzuckerViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Blutzucker");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
