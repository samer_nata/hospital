package com.example.myapplication.ui.Erinnerung;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class ErinnerungViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public ErinnerungViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Erinnerung");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
