package com.example.myapplication.ui.gesam;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class GesamViewModel extends ViewModel {
    private MutableLiveData<String> mText;

    public GesamViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Gesamtübersicht");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
