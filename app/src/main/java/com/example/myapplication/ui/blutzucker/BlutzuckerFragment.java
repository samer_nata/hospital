package com.example.myapplication.ui.blutzucker;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.myapplication.R;

public class BlutzuckerFragment extends Fragment {
    private BlutzuckerViewModel blutdruckViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        blutdruckViewModel =
                ViewModelProviders.of(this).get(BlutzuckerViewModel.class);
        View root = inflater.inflate(R.layout.fragment_blutzucker, container, false);

        final TextView textView = root.findViewById(R.id.text_blutzucker);
        blutdruckViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}
