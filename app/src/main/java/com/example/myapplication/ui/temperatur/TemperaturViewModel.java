package com.example.myapplication.ui.temperatur;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class TemperaturViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public TemperaturViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Temperatur");
    }

    public LiveData<String> getText() {
        return mText;
    }
}